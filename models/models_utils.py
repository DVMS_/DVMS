import torch


def metric_orth_dist(position_a, position_b):
    # Normalize onto the unit sphere
    position_a /= torch.linalg.norm(position_a, dim=-1).unsqueeze(-1).repeat(1, 1, 3)
    position_b /= torch.linalg.norm(position_b, dim=-1).unsqueeze(-1).repeat(1, 1, 3)
    # Finally compute orthodromic distance
    great_circle_distance = 2 * torch.asin(torch.linalg.norm(position_b - position_a, dim=-1) / 2)
    return great_circle_distance


def flat_top_k_orth_dist(k_position_a, position_b, k):
    batch_size, seq_len, _ = position_b.shape
    k_position_b = torch.repeat_interleave(position_b, k, dim=0)
    k_orth_dist = metric_orth_dist(k_position_a, k_position_b).reshape((batch_size, k, seq_len))
    _, best_orth_dist_idx = torch.min(torch.mean(k_orth_dist, dim=-1), dim=-1)
    best_orth_dist = k_orth_dist[range(batch_size), best_orth_dist_idx]
    return best_orth_dist


def to_position_normalized(values):
    orientation = values[0]
    motion = values[1]
    result = orientation + motion
    return result / torch.norm(result, dim=-1).reshape(-1, 1, 1).repeat(1, 1, 3)
