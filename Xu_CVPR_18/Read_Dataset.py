import sys

sys.path.insert(0, './')

import os
import pandas as pd


TRAIN_TEST_SET_FILE = 'dataset/train_test_set.xlsx'

# Read the split of the video ids for the train and test set in the experiments from CVPR18
def get_videos_train_and_test_from_file(root_folder):
    xl_train_test_set_file = pd.ExcelFile(os.path.join(root_folder, TRAIN_TEST_SET_FILE), engine='openpyxl')
    df_train = xl_train_test_set_file.parse('train_set', header=None)
    df_test = xl_train_test_set_file.parse('test_set', header=None)
    videos_train = df_train[0].values
    videos_test = df_test[0].values
    videos_train = ['%03d' % video_id for video_id in videos_train]
    videos_test = ['%03d' % video_id for video_id in videos_test]
    return videos_train, videos_test
