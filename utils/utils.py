import pandas as pd


def load_dict_from_csv(filename, sep=',', header=0, engine='python'):
    dataframe = pd.read_csv(filename, engine=engine, header=header, sep=sep, dtype=str)
    return dataframe.values
