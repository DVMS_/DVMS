import os
import numpy as np
from torch.utils.data import Dataset

from Xu_CVPR_18.Read_Dataset import get_videos_train_and_test_from_file
from utils.sampled_dataset import get_video_ids, get_user_ids, get_users_per_video, partition_in_train_and_test_without_video_intersection, partition_in_train_and_test, split_list_by_percentage, partition_in_train_and_test_without_any_intersection, read_sampled_positions_for_trace
from utils.utils import load_dict_from_csv


class CustomDataset(Dataset):
    MODELS_USING_SALIENCY = []

    def __init__(self, model_name, all_traces, list_IDs, m_window, h_window):
        self.model_name = model_name
        self.all_traces = all_traces
        self.list_IDs = list_IDs
        self.m_window = m_window
        self.h_window = h_window

    def __len__(self):
        return len(self.list_IDs)

    def __getitem__(self, index):
        ID = self.list_IDs[index]
        user = ID['user']
        video = ID['video']
        x_i = ID['time-stamp']
        # Load the data
        encoder_pos_inputs = self.all_traces[video][user][x_i - self.m_window:x_i].astype(np.float32)
        decoder_pos_inputs = self.all_traces[video][user][x_i:x_i + 1].astype(np.float32)
        decoder_outputs = self.all_traces[video][user][x_i + 1:x_i + self.h_window + 1].astype(np.float32)

        if self.model_name in ['DVMS']:
            return [encoder_pos_inputs, decoder_pos_inputs], decoder_outputs, video, user, x_i


class DatasetLoader:
    def __init__(self, dataset_name, model_name, m_window, h_window, init_window=None, end_window=None, provided_videos=True):

        assert dataset_name in ['Xu_PAMI_18', 'Xu_CVPR_18', 'David_MMSys_18']

        assert model_name in ['DVMS']

        self.dataset_name = dataset_name
        self.model_name = model_name
        self.m_window = m_window
        self.h_window = h_window
        self.init_window = init_window
        self.end_window = end_window
        self.provided_videos = provided_videos

        self.root_dataset_folder = os.path.join('./', dataset_name)

        self.exp_name = f'_init_{init_window}_in_{m_window}_out_{h_window}_end_{end_window}'
        self.sampled_dataset_folder = os.path.join(self.root_dataset_folder, 'sampled_dataset')

        self.results_folder = os.path.join(self.root_dataset_folder, self.model_name, 'Results' + self.exp_name)
        self.models_folder = os.path.join(self.root_dataset_folder, self.model_name, 'Models' + self.exp_name)

        self.perc_videos_train = 0.8
        self.perc_users_train = 0.5

    def load(self):
        videos = get_video_ids(self.sampled_dataset_folder)
        users = get_user_ids(self.sampled_dataset_folder)
        users_per_video = get_users_per_video(self.sampled_dataset_folder)

        if self.provided_videos:
            if self.dataset_name == 'Xu_CVPR_18':
                videos_train, videos_test = get_videos_train_and_test_from_file(self.root_dataset_folder)
                partition = partition_in_train_and_test_without_video_intersection(self.sampled_dataset_folder, self.init_window, self.end_window, videos_train, videos_test, users_per_video)
            elif self.dataset_name == 'Xu_PAMI_18':
                # From Xu_PAMI_18 paper:
                # For evaluating the performance of offline-DHP, we randomly divided all 76 panoramic sequences of our PVS-HM database into a training set (61 sequences) and a test set (15 sequences).
                # For evaluating the performance of online-DHP [...]. Since the DRL network of offline-DHP was learned over 61 training sequences and used as the initial model of online-DHP, our comparison was conducted on all 15 test sequences of our PVS-HM database.
                videos_test = ['KingKong', 'SpaceWar2', 'StarryPolar', 'Dancing', 'Guitar', 'BTSRun', 'InsideCar', 'RioOlympics', 'SpaceWar', 'CMLauncher2', 'Waterfall', 'Sunset', 'BlueWorld', 'Symphony', 'WaitingForLove']
                videos_train = ['A380', 'AcerEngine', 'AcerPredator', 'AirShow', 'BFG', 'Bicycle', 'Camping', 'CandyCarnival', 'Castle', 'Catwalks', 'CMLauncher', 'CS', 'DanceInTurn', 'DrivingInAlps', 'Egypt', 'F5Fighter', 'Flight', 'GalaxyOnFire', 'Graffiti', 'GTA', 'HondaF1', 'IRobot', 'KasabianLive', 'Lion', 'LoopUniverse', 'Manhattan', 'MC', 'MercedesBenz', 'Motorbike', 'Murder', 'NotBeAloneTonight', 'Orion', 'Parachuting', 'Parasailing', 'Pearl', 'Predator', 'ProjectSoul', 'Rally', 'RingMan', 'Roma', 'Shark', 'Skiing', 'Snowfield', 'SnowRopeway', 'Square', 'StarWars', 'StarWars2', 'Stratosphere', 'StreetFighter', 'Supercar', 'SuperMario64', 'Surfing', 'SurfingArctic', 'TalkingInCar', 'Terminator', 'TheInvisible', 'Village', 'VRBasketball', 'Waterskiing', 'WesternSichuan', 'Yacht']
                partition = partition_in_train_and_test_without_video_intersection(self.sampled_dataset_folder, self.init_window, self.end_window, videos_train, videos_test, users_per_video)
            elif self.dataset_name in ['David_MMSys_18']:
                train_traces = load_dict_from_csv(os.path.join(self.root_dataset_folder, 'train_set'))
                test_traces = load_dict_from_csv(os.path.join(self.root_dataset_folder, 'test_set'))
                partition = partition_in_train_and_test(self.sampled_dataset_folder, self.init_window, self.end_window, train_traces, test_traces)
            else:
                raise ValueError(f"Dataset {self.dataset_name} not supported!")
        else:
            videos_train, videos_test = split_list_by_percentage(videos, self.perc_videos_train)
            users_train, users_test = split_list_by_percentage(users, self.perc_users_train)
            partition = partition_in_train_and_test_without_any_intersection(self.sampled_dataset_folder, self.init_window, self.end_window, videos_train, videos_test, users_train, users_test)
        all_traces = {}
        for video in videos:
            all_traces[video] = {}
            for user in users_per_video[video]:
                all_traces[video][user] = read_sampled_positions_for_trace(self.sampled_dataset_folder, str(video), str(user))
        return CustomDataset(self.model_name, all_traces, partition['train'], self.m_window, self.h_window), CustomDataset(self.model_name, all_traces, partition['test'], self.m_window, self.h_window)
