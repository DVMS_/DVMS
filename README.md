# Deep Variational Learning for Multiple Trajectory Prediction of 360° Head Movements

 <img src="https://www.acm.org/binaries/content/gallery/acm/publications/artifact-review-v1_1-badges/artifacts_evaluated_reusable_v1_1.png" alt="ACM badge: Artifacts Evaluated & Reusable / v1.1" width="100"> 
 <img src="https://www.acm.org/binaries/content/gallery/acm/publications/artifact-review-v1_1-badges/artifacts_available_v1_1.png" alt="ACM badge: Artifacts Available / v1.1" width="100"> 
 <img src="https://www.acm.org/binaries/content/gallery/acm/publications/artifact-review-v1_1-badges/results_reproduced_v1_1.png" alt="ACM badge: Results Reproduced / v1.1" width="100"> 

## Citing this work

```
@inproceedings{10.1145/3524273.3528176,
author = {Guimard, Quentin and Sassatelli, Lucile and Marchetti, Francesco and Becattini, Federico and Seidenari, Lorenzo and Del Bimbo, Alberto},
title = {Deep Variational Learning for Multiple Trajectory Prediction of 360° Head Movements},
year = {2022},
isbn = {9781450392839},
publisher = {Association for Computing Machinery},
address = {New York, NY, USA},
url = {https://doi.org/10.1145/3524273.3528176},
doi = {10.1145/3524273.3528176},
booktitle = {Proceedings of the 13th ACM Multimedia Systems Conference},
pages = {12–26},
numpages = {15},
keywords = {360° videos, trajectory prediction, deep learning, head motion},
location = {Athlone, Ireland},
series = {MMSys '22}
}
```

## Authors

- [Quentin Guimard](mailto:quentin.guimard@univ-cotedazur.fr) - Université Côte d'Azur, CNRS, I3S
- Lucile Sassatelli - Université Côte d'Azur, CNRS, I3S, Institut Universitaire de France
- Francesco Marchetti - Università degli Studi di Firenze, MICC
- Federico Becattini - Università degli Studi di Firenze, MICC
- Lorenzo Seidenari - Università degli Studi di Firenze, MICC
- Alberto Del Bimbo - Università degli Studi di Firenze, MICC

## Repository structure

### Datasets

The head motion traces datasets that we used to train and evaluate our model are directly included in this repository. They were all taken from the repository of [Romero et al.](https://gitlab.com/miguelfromeror/head-motion-prediction/-/tree/master/) and were all already resampled to 5 Hz.
- `./David_MMSys_18/` contains the dataset from [David et al.](https://dl.acm.org/doi/10.1145/3204949.3208139).
- `./Xu_CVPR_18/` contains the dataset from [Xu et al.](https://ieeexplore.ieee.org/document/8578657).
- `./Xu_PAMI_18/` contains the dataset from [Xu et al.](https://ieeexplore.ieee.org/document/8418756).

### Model

The model was implemented using PyTorch and is located in `./models/dvms.py`.

### Training procedure

The file `./training_procedure.py` contains everything that is need to train and/or test the model. There are many available parameters that can be explored by typing `python ./training_procedure.py -h`.

## Quickstart using Docker (optional)

You can use [nvidia-docker](https://github.com/NVIDIA/nvidia-docker) and pull our image with all the libraries installed if you have a CUDA-capable GPU.
If you do not wish to use Docker, please skip to [Preparing a virtual environment (optional)](#preparing-a-virtual-environment-optional).

### Detailed steps

First, make sure you have installed the NVIDIA driver and Docker engine for your Linux distribution. Note that you do not need to install the CUDA Toolkit on the host system, but the NVIDIA driver needs to be installed.

Install nvidia-docker by following [this link](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker).

```shell
git clone https://gitlab.com/DVMS_/DVMS.git /path/to/DVMS
docker pull qguim/dvms_env
docker run -d -it --rm --runtime=nvidia -v /path/to/DVMS:/DVMS -p 8888:8888 --name DVMS qguim/dvms_env
docker attach DVMS
cd ../DVMS/
./train.sh # To train the model (see "Scripts" section below)
./evaluate.sh # To evaluate the model (see "Scripts" section below)
jupyter notebook --allow-root --ip 0.0.0.0 # To visualize the results (see "Notebook" section below)
```

## Preparing a virtual environment (optional)

It is recommended to use a Python virtual environment to install the required libraries. You can create and activate a new virtual environment using `venv`:
- To create the environment, execute `python3 -m venv <path/to/environment>`
- To activate the environment, execute `source <path/to/environment>/bin/activate` if you are using bash/zsh on a POSIX system.

For other platforms and further information on `venv`, please refer to the [official Python documentation](https://docs.python.org/3/library/venv.html).

## Requirements

A pip requirements file can be found at the root: `./requirements.txt`. The required libraries to run all the scripts and the notebook can be installed by running `python -m pip install -r ./requirements.txt`.
The versions specified in this file are the ones that have been tested to work with our code, but other versions may work.
Regarding the version of PyTorch that is specified, `+cu110` refers to the CUDA version, you may change this if you have another version of CUDA install. If you do not have CUDA install or do not want to use CUDA, please refer to [Running without a GPU](#running-without-a-gpu).

## Model weights

We provide the trained weights of the model that matches the values reported in the paper in `<dataset_name>/DVMS/Models_init_30_in_5_out_25_end_25_2_64_128_64_0.0005_<K>/weights.pth`, where `<dataset_name>` is one of the three datasets mentioned above and `<K>` is the number of predicted trajectories.
These weights can be tested with the `./evaluate.sh` script, but the model can also be re-trained using the `./train.sh` script.

## Scripts

Scripts with examples of commands to train (`./train.sh`) and test (`./evaluate.sh`) the model are provided.
Running both scripts without modification will train and test the model on all 3 datasets with the same parameters as we did for the paper.
- Running `./train.sh` will create/erase the weights file present in `<dataset_name>/DVMS/Models_init_30_in_5_out_25_end_25_2_64_128_64_0.0005_<K>/weights.pth` and the log file in `<dataset_name>/DVMS/Results_init_30_in_5_out_25_end_25_2_64_128_64_0.0005_<K>/console.log`.
- Running `./evaluate.sh` will create/erase the error file present in `<dataset_name>/DVMS/Results_init_30_in_5_out_25_end_25_2_64_128_64_0.0005_<K>/errors.pkl` and the trajectory file in `<dataset_name>/DVMS/Results_init_30_in_5_out_25_end_25_2_64_128_64_0.0005_<K>/trajectories.pkl`.

The log file is a copy of the output during the training of the model. The error file contains the orthodromic distances between the predicted and ground-truth future trajectories for all timesteps of all samples of the test set. The trajectory file contains the outputs of the network for all samples of the test set.

## Running without a GPU

The model may be trained and tested without a GPU, albeit much more slowly. To use the CPU instead of the GPU, just remove the option `--gpu_id <GPU_ID>` from the command when executing `./training_procedure.py`.
If you do not have a GPU or CUDA, you need to change the Pytorch version in the requirements file to `torch==1.7.1+cpu`.

## Notebook

Parsing the error files can be done using the notebook in `./notebook/visualize_error.ipynb`. For every dataset, all the error files are parsed and the results are shown in the form of a plot like in Fig. 8 of the paper, and in the form of a table like Tables 1 and 2 of the paper.

To run the notebook, first start a Jupyter server by running the command `jupyter notebook` with your virtual environment activated. The server should appear in your browser at `localhost:8888` (you can also specify another port with the option `--port <PORT_ID>` when starting the server). You can then select and run the notebook. More information on Jupyter [here](https://jupyter.org/).
