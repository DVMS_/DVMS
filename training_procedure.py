import numpy as np
import torch
from torch.utils.data import DataLoader
from torch.optim import AdamW
import os
import pandas as pd
import pickle
import sys
from tqdm.auto import tqdm
import argparse

sys.path.insert(0, './')

from models import DVMS
from models.models_utils import flat_top_k_orth_dist
from utils.load_dataset import DatasetLoader
from utils.save_output import Tee

BATCH_SIZE = 64
LEARNING_RATE = 5e-4
EPOCHS = 80
K = 1
DIMENSIONS = 3

parser = argparse.ArgumentParser(description='Process the input parameters to train the network.')

parser.add_argument('--train', action="store_true", dest='train_flag',
                    help='Train the model.')
parser.add_argument('--evaluate', action="store_true", dest='evaluate_flag',
                    help='Save error and output trajectories on test split of the dataset.')
parser.add_argument('--gpu_id', action='store', dest='gpu_id', help='The gpu used to train this network.')
parser.add_argument('--dataset_name', action='store', dest='dataset_name',
                    help='The name of the dataset used to train this network.')
parser.add_argument('--model_name', action='store', dest='model_name',
                    help='The name of the model used to reference the network structure used.')
parser.add_argument('--train_weights', action='store', dest='train_weights',
                    help='(Optional) Path to model weights to train from instead of training from scratch.')
parser.add_argument('--evaluate_model_dir', action='store', dest='evaluate_model_dir',
                    help='(Optional) Path to the model to be evaluated.')
parser.add_argument('--evaluate_output_dir', action='store', dest='evaluate_output_dir',
                    help='(Optional) Path to save the error file of the evaluated model.')
parser.add_argument('--init_window', action='store', dest='init_window',
                    help='(Optional) Initial buffer window (to avoid stationary part).', type=int)
parser.add_argument('--m_window', action='store', dest='m_window', help='Past history window.', type=int)
parser.add_argument('--h_window', action='store', dest='h_window', help='Prediction window.', type=int)
parser.add_argument('--end_window', action='store', dest='end_window',
                    help='(Optional) Final buffer (to avoid having samples with less outputs).', type=int)
parser.add_argument('--provided_videos', action="store_true", dest='provided_videos',
                    help='Flag that tells whether the list of videos is provided in a global variable.')
parser.add_argument('--lr', action="store", dest='lr', help='(Optional) Neural network learning rate.')
parser.add_argument('--bs', action="store", dest='bs', help='(Optional) Neural network batch size.')
parser.add_argument('-K', action="store", dest='K',
                    help='(Optional) Number of predicted trajectories (default to 1).')

args = parser.parse_args()

dataset_name = args.dataset_name
model_name = args.model_name

assert dataset_name in ['Xu_PAMI_18', 'Xu_CVPR_18', 'David_MMSys_18']
assert model_name in ['DVMS']

M_WINDOW = args.m_window
H_WINDOW = args.h_window

if args.gpu_id is None:
    device = torch.device('cpu')
    print('WARNING: No GPU selected.')
else:
    try:
        device = torch.device(f'cuda:{int(args.gpu_id)}')
    except TypeError:
        device = torch.device('cpu')
        print('WARNING: No GPU selected.')

if args.init_window is None:
    INIT_WINDOW = M_WINDOW
else:
    INIT_WINDOW = args.init_window

if args.end_window is None:
    END_WINDOW = H_WINDOW
else:
    END_WINDOW = args.end_window

if args.bs is not None:
    BATCH_SIZE = int(args.bs)

if args.lr is not None:
    LEARNING_RATE = float(args.lr)

if args.K is not None:
    K = int(args.K)

TRAIN_MODEL = False
EVALUATE_MODEL = False

if args.train_flag:
    TRAIN_MODEL = True
if args.evaluate_flag:
    EVALUATE_MODEL = True

dataset_loader = DatasetLoader(dataset_name, model_name, M_WINDOW, H_WINDOW, INIT_WINDOW, END_WINDOW, args.provided_videos)

if model_name == 'DVMS':
    model = DVMS(DIMENSIONS, H_WINDOW, n_samples_train=K, device=device).float().to(device)
    dataset_loader.models_folder += f'_{model.n_hidden}_{model.hidden_dim}_{model.latent_dim}_{BATCH_SIZE}_{LEARNING_RATE}_{K}'
    dataset_loader.results_folder += f'_{model.n_hidden}_{model.hidden_dim}_{model.latent_dim}_{BATCH_SIZE}_{LEARNING_RATE}_{K}'
    metrics = {"top_k_orth_dist": lambda *x: flat_top_k_orth_dist(*x, K)}
    metrics_test = {"top_k_orth_dist": lambda *x: flat_top_k_orth_dist(*x, K)}
    val_metric = "top_k_orth_dist"

if TRAIN_MODEL:
    if not os.path.exists(dataset_loader.models_folder):
        os.makedirs(dataset_loader.models_folder)
    if not os.path.exists(dataset_loader.results_folder):
        os.makedirs(dataset_loader.results_folder)

    if args.train_weights is not None:
        model.load_state_dict(torch.load(args.train_weights + '/weights.pth', map_location=device))

    output_log = open(dataset_loader.results_folder + '/console.log', 'w')
    sys.stdout = Tee(sys.__stdout__, output_log)

    print(f'Loading {dataset_name} dataset...')
    train_dataset, test_dataset = dataset_loader.load()
    train_dataloader = DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True, pin_memory=True)
    test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE, shuffle=False, pin_memory=True)
    train_size = len(train_dataset)
    test_size = len(test_dataset)
    print('Loading finished.')

    optimizer = AdamW(model.parameters(), lr=LEARNING_RATE)

    print(f'Training {model_name} on {dataset_name} - K: {K} - Batch size: {BATCH_SIZE} - Learning rate: {LEARNING_RATE}')
    for epoch in range(EPOCHS):
        print(f"Epoch {epoch + 1}/{EPOCHS}\n-------------------------------")
        # TRAINING
        model.train()
        metrics_val = {}
        metrics_vals = {}
        for batch, (X, y, video, user, x_i) in enumerate(train_dataloader):
            X, y = [x.float().to(device) for x in X], y.float().to(device)
            batch_size = y.shape[0]
            # Compute prediction and loss
            results = model([X, y])
            if torch.isnan(results[0]).any():
                print('`\nNan detected in model output!')
                break
            train_loss = model.loss_function(*results)
            for name, value in train_loss.items():
                metrics_val[name] = torch.mean(value.detach()).item()
                try:
                    metrics_vals[name].append(metrics_val[name] * (batch_size / train_size))
                except KeyError:
                    metrics_vals[name] = [metrics_val[name] * (batch_size / train_size)]
            for name, func in metrics.items():
                metrics_val[name] = torch.mean(func(results[0].detach(), y)).item()
                try:
                    metrics_vals[name].append(metrics_val[name] * (batch_size / train_size))
                except KeyError:
                    metrics_vals[name] = [metrics_val[name] * (batch_size / train_size)]

            # Backpropagation
            optimizer.zero_grad()
            train_loss['loss'].backward()
            optimizer.step()

            # Print running loss and metrics
            print(f"\r[{batch + 1:>5d}/{int(np.ceil(train_size / BATCH_SIZE)):>5d}]", end='')
            for name, value in metrics_val.items():
                print(f" - {name}: {value:>7f}", end='')
        else:
            # Print average training loss and metrics
            print(f"\r[{int(np.ceil(train_size / BATCH_SIZE)):>5d}/{int(np.ceil(train_size / BATCH_SIZE)):>5d}]",
                  end='')
            for name, value_list in metrics_vals.items():
                print(f" - {name}: {np.sum(value_list):>7f}", end='')

            # VALIDATION
            model.eval()
            metrics_val = {}
            metrics_vals = {}
            with torch.no_grad():
                for X, y, video, user, x_i in test_dataloader:
                    X, y = [x.float().to(device) for x in X], y.float().to(device)
                    batch_size = y.shape[0]
                    if model_name in ['DVMS']:
                        results = model.sample(X)
                    for name, func in metrics_test.items():
                        metrics_val[name] = torch.mean(func(results, y)).item()
                        try:
                            metrics_vals[name].append(metrics_val[name] * (batch_size / test_size))
                        except KeyError:
                            metrics_vals[name] = [metrics_val[name] * (batch_size / test_size)]

            for name, value_list in metrics_vals.items():
                test_metric = np.sum(value_list)
                print(f"\nval_{name}: {test_metric:>8f}", end='')
                metrics_vals[name] = test_metric
            print()
            if epoch == 0:
                best_val_loss = metrics_vals[val_metric]
                torch.save(model.state_dict(), dataset_loader.models_folder + '/weights.pth')
            else:
                if metrics_vals[val_metric] < best_val_loss:
                    best_val_loss = metrics_vals[val_metric]
                    torch.save(model.state_dict(), dataset_loader.models_folder + '/weights.pth')
            continue
        print('Training has been interrupted.')
        break

    print(f'Training finished.')

if EVALUATE_MODEL:
    with torch.no_grad():
        print(f'Loading {dataset_name} dataset...')
        _, test_dataset = dataset_loader.load()
        test_dataloader = DataLoader(test_dataset, batch_size=BATCH_SIZE * 16, shuffle=False, pin_memory=True)
        print('Loading finished.')

        if args.evaluate_model_dir is None:
            model_dir = dataset_loader.models_folder
        else:
            model_dir = args.evaluate_model_dir

        if args.evaluate_output_dir is None:
            output_dir = model_dir.replace('Models', 'Results')
        else:
            output_dir = args.evaluate_output_dir
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        print(f'Loading {model_name} model...')
        model.load_state_dict(torch.load(model_dir + '/weights.pth', map_location=device))
        print('Loading finished.')

        errors = []
        trajectories = []
        model.eval()

        print(f'Evaluating {model_name} on {dataset_name} - K: {K}')

        for batch, (X, y, video, user, x_i) in enumerate(tqdm(test_dataloader)):
            X, y = [x.float().to(device) for x in X], y.float().to(device)
            batch_size = y.shape[0]
            if model_name in ['DVMS']:
                results = model.sample(X)
            orth_dist = flat_top_k_orth_dist(results, y, K).squeeze().cpu().numpy()
            past = torch.cat(X, 1)
            results = results.reshape(batch_size, K, H_WINDOW, DIMENSIONS)
            for i in range(batch_size):
                trajectories.append({'video': video[i], 'user': user[i], 'x_i': x_i[i].cpu().item(), 'past': past[i].cpu().numpy(), 'future': y[i].cpu().numpy(), 'predicted': results[i].cpu().numpy()})
                for t in range(H_WINDOW):
                    errors.append({'video': video[i], 'user': user[i], 'x_i': x_i[i].cpu().item(), 't': (t + 1) * 0.2, 'orthodromic_distance': orth_dist[i][t].item()})

        pickle.dump(errors, open(output_dir + f'/errors.pkl', 'wb'))
        pickle.dump(trajectories, open(output_dir + f'/trajectories.pkl', 'wb'))
        print(f'Evaluation finished.')

        df = pd.DataFrame.from_records(errors)
        ADE_str = ''
        for t in range(5):
            ADE_str += f"ADE {t + 1}s: {df[df['t'] <= t + 1]['orthodromic_distance'].mean():.3f} - "
        print(ADE_str[:-3])

        print(f'Results saved in {output_dir}.')

