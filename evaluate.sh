## MMSys18 dataset
python training_procedure.py --evaluate --gpu_id 0 --dataset_name David_MMSys_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 1
python training_procedure.py --evaluate --gpu_id 0 --dataset_name David_MMSys_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 2
python training_procedure.py --evaluate --gpu_id 0 --dataset_name David_MMSys_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 3
python training_procedure.py --evaluate --gpu_id 0 --dataset_name David_MMSys_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 4
python training_procedure.py --evaluate --gpu_id 0 --dataset_name David_MMSys_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 5

## CVPR18 dataset
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_CVPR_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 1
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_CVPR_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 2
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_CVPR_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 3
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_CVPR_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 4
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_CVPR_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 5

## PAMI18 dataset
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_PAMI_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 1
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_PAMI_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 2
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_PAMI_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 3
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_PAMI_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 4
python training_procedure.py --evaluate --gpu_id 0 --dataset_name Xu_PAMI_18 --model_name DVMS --m_window 5 --h_window 25 --init_window 30 --provided_videos -K 5
